import React, { Component } from "react";

class Course extends Component {
  render() {
    console.log(this.props);
    const title = this.props.location.search.split("=");
    return (
      <div>
        <h1>{decodeURIComponent(title[1])}</h1>
        <p>You selected the Course with ID:{this.props.match.params.cid}</p>
      </div>
    );
  }
}

export default Course;
